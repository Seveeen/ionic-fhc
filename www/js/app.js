// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'ngAnimate'])

.run(function ($ionicPlatform) {
    $ionicPlatform.ready(function () {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (cordova.platformId === 'ios' && window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }

    });
})

.config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
    $ionicConfigProvider.views.transition('platform');
    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider

    // setup an abstract state for the tabs directive
      .state('tab', {
          url: '/tab',
          abstract: true,
          templateUrl: function () {
              if (ionic.Platform.isAndroid()) return 'templates/tabs-android.html';
              else if (ionic.Platform.isIOS()) return 'templates/tabs-ios.html';
          }
      })

    // Each tab has its own nav history stack:

    .state('tab.instalaciones', {
        url: '/instalaciones',
        views: {
            'tab-instalaciones': {
                templateUrl: 'templates/tab-instalaciones.html',
                controller: 'InstalacionesCtrl',
            }
        }
    })
  .state('tab.instalacion-detail', {
      url: '/instalaciones/:ventaId',
      views: {
          'tab-instalaciones': {
              templateUrl: 'templates/venta-detail.html',
              controller: 'VentaDetailCtrl'
          }
      }
  })
    .state('tab.ventas', {
        url: '/ventas',
        views: {
            'tab-ventas': {
                templateUrl: 'templates/tab-ventas.html',
                controller: 'VentasCtrl'
            }
        }
    })
      .state('tab.venta-detail', {
          url: '/ventas/:ventaId',
          views: {
              'tab-ventas': {
                  templateUrl: 'templates/venta-detail.html',
                  controller: 'VentaDetailCtrl'
              }
          }
      })
      .state('tab.crear-venta', {
          url: '/ventas/crear',
          views: {
              'tab-ventas': {
                  templateUrl: 'templates/tab-crear-venta.html',
                  controller: 'ventaCtrl'
              }
          }
      })
    .state('tab.account', {
        url: '/account',
        views: {
            'tab-account': {
                templateUrl: 'templates/tab-account.html',
                controller: 'AccountCtrl'
            }
        }
    });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/tab/instalaciones');

});
