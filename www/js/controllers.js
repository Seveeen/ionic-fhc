angular.module('starter.controllers', [])

.controller('globalController', function ($scope, $scope, $ionicModal, $ionicTabsDelegate, Ventas, User) {
    $scope.user = User.get();
    $scope.ventas = Ventas.all();
    $scope.goForward = function () {
        var selected = $ionicTabsDelegate.selectedIndex();
        if (selected != -1) {
            $ionicTabsDelegate.select(selected + 1);
        }
    }

    $scope.goBack = function () {
        var selected = $ionicTabsDelegate.selectedIndex();
        if (selected != -1 && selected != 0) {
            $ionicTabsDelegate.select(selected - 1);
        }
    }

    if (ionic.Platform.isIOS()) {
        $scope.platform_theme = "stable";
        $scope.platform_name = "ios";
        $scope.nueva_venta_icon = "ios-plus-empty";

    }
    else {
        $scope.platform_theme = "positive";
        $scope.platform_name = "android";
        $scope.nueva_venta_icon = "android-create";
    }

    $ionicModal.fromTemplateUrl('login.html', function (modal) {
        $scope.loginModal = modal;
    }, {
        scope: $scope,
        animation: 'slide-in-up'
    });

    $scope.openLogin = function () {
        $scope.loginModal.show();
    };
    $scope.closeLogin = function () {
        $scope.loginModal.hide();
    };

    ionic.Platform.ready(function () {
        if (!window.localStorage.getItem("MDisLoggedin")) $scope.openLogin();
    });
}).controller('InstalacionesCtrl', function ($scope, $ionicModal, Ventas, User) {
    $scope.ventasInstalandose = Ventas.instalandose();
    var usuario = User.get();
})

.controller('VentasCtrl', function ($scope, $ionicModal, $ionicPlatform, User) {
    // With the new view caching in Ionic, Controllers are only called
    // when they are recreated or on app start, instead of every page change.
    // To listen for when this page is active (for example, to refresh data),
    // listen for the $ionicView.enter event:
    //
    //$scope.$on('$ionicView.enter', function(e) {
    //});
    $ionicModal.fromTemplateUrl('createVenta.html', function (modal) {
        $scope.ventaModal = modal;
    }, {
        scope: $scope,
        animation: 'slide-in-up'
    });

    $scope.ventasModel = { toggleFiltro: false };
    $scope.filtro = { id_delegado: "" };
    $scope.openVenta = function () {
        $scope.ventaModal.show();
    };
    $scope.closeVenta = function () {
        $scope.ventaModal.hide();
    };
    $scope.filtrarVentas = function () {
        if ($scope.ventasModel.toggleFiltro) {
            var usuario = User.get();
            $scope.filtro.id_delegado = usuario[0].id_delegado;
        } else {
            $scope.filtro.id_delegado = "";
        }
    };
})

.controller('VentaDetailCtrl', function ($scope, $stateParams, Ventas) {
    $scope.venta = Ventas.get($stateParams.ventaId);
})

.controller('AccountCtrl', function ($scope, User) {
    $scope.user = User.get();
})
.controller('ventaCtrl', function ($scope) {

});